#!/usr/bin/env python3.9

import tkinter
from tkinter import ttk
import tkinter.font as tkFont

# Defing Window Information
root = tkinter.Tk()
root.title('Base Converter')
root.geometry('610x200')
root.resizable(False, False)
root.attributes('-topmost', 1)

# Fonts
font1 = tkinter.font.Font(
        family="Helvetica",
        size=20)

# Creating num variable
num = tkinter.StringVar(root)
binVar = tkinter.StringVar()
octVar = tkinter.StringVar()
decVar = tkinter.StringVar()
hexVar = tkinter.StringVar()

def baseConvert():
    tmpNum = str(num.get())
    if tmpNum[:2] == '0b':
        decNum = int(tmpNum[2:], 2)
    elif tmpNum[:2] == '0o':
        decNum = int(tmpNum[2:], 8)
    elif tmpNum[:2] == '0x':
        decNum = int(tmpNum[2:], 16)
    elif tmpNum.isnumeric():
        decNum = int(tmpNum)
    elif tmpNum == '':
        pass
    else:
        exit("Please run this correctly.")


    binVar.set(str(bin(decNum))[2:])
    octVar.set(str(oct(decNum))[2:])
    decVar.set(decNum)
    hexVar.set(str(hex(decNum))[2:].upper())

# Cleaning The existing 
#def clearTextboxes():
#   binEntry.delete(1.0, "end")
#   octEntry.delete(1.0, "end")
#   decEntry.delete(1.0, "end")
#   hexEntry.delete(1.0, "end")

#   baseConv()

# Base Input
numEntry = ttk.Entry(root,
                width=35,
                textvariable=num, 
                font='Hack-Nerd-Font 15')

numEntry.configure(justify='right')
numEntry.grid(row=1, column=1, sticky='e')
numEntry.focus()

cnvtButton = ttk.Button(root,
                 text="Convert",
                 command=baseConvert
                 )
cnvtButton.grid(row=1, column=2, pady=8)

# Binary Output
binEntry = tkinter.Label(root,
                    width=35,
                    height=1,
                    textvariable=binVar,
                    relief=tkinter.SUNKEN,
                    justify="right",
                    bd=1,
                    bg="brown", fg="white",
                    font="Hack-Nerd-Font 20")
binEntry.grid(row=2, column=1, columnspan=2, pady=3, padx=5)

# Octal Output
octEntry = tkinter.Label(root,
                    width=35,
                    height=1, 
                    textvariable=octVar,
                    relief=tkinter.SUNKEN,
                    justify="right",
                    bd=1,
                    bg="brown", fg="white",
                    font="Hack-Nerd-Font 20")
octEntry.grid(row=3, column=1, columnspan=2, sticky='e', padx=6)

# Decimal Output
decEntry = tkinter.Label(root,
                    width=35,
                    height=1, 
                    textvariable=decVar,
                    relief=tkinter.SUNKEN,
                    justify="right",
                    bd=1,
                    bg="brown", fg="white",
                    font="Hack-Nerd-Font 20")
decEntry.grid(row=4, column=1, columnspan=2, pady=3, padx=6)

# Hex Output
hexEntry = tkinter.Label(root,
                    width=35,
                    height=1, 
                    textvariable=hexVar,
                    relief=tkinter.SUNKEN,
                    justify="right",
                    bd=1,
                    bg="brown", fg="white",
                    font="Hack-Nerd-Font 20")

hexEntry.grid(row=5, column=1, columnspan=2, padx=6)

root.mainloop()
