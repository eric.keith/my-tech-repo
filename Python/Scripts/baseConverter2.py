#!/usr/bin/env python3.9

import tkinter
from tkinter import ttk
import tkinter.font as tkFont

# Defing Window Information
root = tkinter.Tk()
root.title('Base Converter')
root.geometry('610x200')
root.resizable(False, False)
root.attributes('-topmost', 1)

# Fonts
font1 = tkinter.font.Font(
        family="Helvetica",
        size=20)


# Creating num variable
num = tkinter.StringVar(root)
binVar = tkinter.StringVar()
octVar = tkinter.StringVar()
decVar = tkinter.StringVar()
hexVar = tkinter.StringVar()


### -------- Conversion Functions -------- ###
def decConvert(num):

    if num[0] == '%':
        decNum = int(num[1:], 2)
    elif num[:2] == '0b':
        decNum = int(num[2:], 2)
    elif num[:2] == '0o':
        decNum = int(num[2:], 8)
    elif num[0] == '$':
        decNum = int(num[1:], 16)
    elif num[:2] == '0x':
        decNum = int(num[2:], 16)
    elif num.isnumeric():
        decNum = num
    else:
        pass

    return str(decNum)


def main():
    
    equation = str(num.get())
    equation = equation.replace('(', '( ').replace(')', ' )')
    lstEquation = equation.split(' ')
    decLstEquation = []

    for x in lstEquation:
        if x in ['+', '-', '/', '*', '(', ')', '^']:
            decLstEquation.append(x)
        else:
            decLstEquation.append(decConvert(x))

    
    finDecNum = eval(' '.join(decLstEquation))

    # Setting Conversion Variables
   #binVar.set(str(bin(finDecNum))[2:])
   #octVar.set(str(oct(finDecNum))[2:])
   #decVar.set(finDecNum)
   #hexVar.set(str(hex(finDecNum))[2:].upper())

    binVar.set(str(bin(finDecNum)))
    octVar.set(str(oct(finDecNum)))
    decVar.set(finDecNum)
    hexVar.set(str(hex(finDecNum)).upper())

### -------- TKinter Settings -------- ###

# Base Input
numEntry = ttk.Entry(root,
                width=35,
                textvariable=num, 
                font='Hack-Nerd-Font 15')

numEntry.configure(justify='right')
numEntry.grid(row=1, column=1, sticky='e')
numEntry.focus()

cnvtButton = ttk.Button(root,
                 text="Convert",
                 command=main
                 )
cnvtButton.grid(row=1, column=2, pady=8)

# Binary Output
binEntry = tkinter.Label(root,
                    width=35,
                    height=1,
                    textvariable=binVar,
                    relief=tkinter.SUNKEN,
                    justify="right",
                    bd=1,
                    bg="brown", fg="white",
                    font="Hack-Nerd-Font 20")
binEntry.grid(row=2, column=1, columnspan=2, pady=3, padx=5)

# Octal Output
octEntry = tkinter.Label(root,
                    width=35,
                    height=1, 
                    textvariable=octVar,
                    relief=tkinter.SUNKEN,
                    justify="right",
                    bd=1,
                    bg="brown", fg="white",
                    font="Hack-Nerd-Font 20")
octEntry.grid(row=3, column=1, columnspan=2, sticky='e', padx=6)

# Decimal Output
decEntry = tkinter.Label(root,
                    width=35,
                    height=1, 
                    textvariable=decVar,
                    relief=tkinter.SUNKEN,
                    justify="right",
                    bd=1,
                    bg="brown", fg="white",
                    font="Hack-Nerd-Font 20")
decEntry.grid(row=4, column=1, columnspan=2, pady=3, padx=6)

# Hex Output
hexEntry = tkinter.Label(root,
                    width=35,
                    height=1, 
                    textvariable=hexVar,
                    relief=tkinter.SUNKEN,
                    justify="right",
                    bd=1,
                    bg="brown", fg="white",
                    font="Hack-Nerd-Font 20")

hexEntry.grid(row=5, column=1, columnspan=2, padx=6)

root.mainloop()
