def decConvert(num):

    if num[0] == '%':
        decNum = int(num[1:], 2)
    elif num[:2] == '0b':
        decNum = int(num[2:], 2)
    elif num[:2] == '0o':
        decNum = int(num[2:], 8)
    elif num[0] == '$':
        decNum = int(num[1:], 16)
    elif num[:2] == '0x':
        decNum = int(num[2:], 16)
    elif num.isnumeric():
        decNum = num
    else:
        pass

    return str(decNum)


if __name__ == '__main__':

    equation = input('Equation: ')
    equation = equation.replace('(', '( ').replace(')', ' )')
    lstEquation = equation.split(' ')
    decLstEquation = []

    for x in lstEquation:
        if x in ['+', '-', '/', '*', '(', ')', '^']:
            decLstEquation.append(x)
        else:
            decLstEquation.append(decConvert(x))
                
    
    print(eval(' '.join(decLstEquation)))
