#!/usr/bin/env python3.9

if __name__ == "__main__":

    for x in range(30):
        binary = bin(x)
        octal = oct(x)
        hexadecimal = hex(x)
        asc = chr(x)
        print(f"{binary:<7} {octal:<4} {x:<2} {hexadecimal:<2} {asc}")
