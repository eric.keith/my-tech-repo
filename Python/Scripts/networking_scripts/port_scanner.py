#!/usr/bin/env python3

from rich.console import Console
from rich.progress import track
from rich.theme import Theme
from rich.tree import Tree
import socket

custom_theme = Theme({"open":"bold green", "closed": "bold red"})
console = Console(theme=custom_theme)
tree = Tree("Opened Ports")


class PortTest:
    def __init__(self, ip_addr, port):
        self.ip_addr = ip_addr
        self.port= port
        self.socket = socket.socket(
                        socket.AF_INET, 
                        socket.SOCK_STREAM
                    )
        self.socket.settimeout(1)
        self.socket.setsockopt(
                        socket.SOL_SOCKET,
                        socket.SO_REUSEADDR,
                        1
                    )

    def run(self):
        response = self.socket.connect_ex((self.ip_addr, self.port))
        return response


if __name__ == "__main__":
    #ip_addr = input("IP ADDR: ")
    ip_addr = "127.0.0.1"

    open_ports = []
    print()
    for port in track(range(1, 1024), description=" Running Port Scan ..."):
        pt = PortTest(ip_addr, port)

        try:
            port_check = pt.run()
            if port_check == 0:
                tree.add(f"[[open]*[/open]] Port {port} is [open]open[/open]")

        except KeyboardInterrupt:
            print("\n Exiting Now")

    console.print(tree)
    """
    if len(open_ports) > 0:
        console.print(tree)
    else:
        console.print("[closed]--- No Open Ports ---[/closed]")
    """
