# To Do's
- [ ] Remove Elastic Stack directory until needed
- [ ] Rename **_Monitoring Your Home Network with Elastic_** to _**Monitor Your Home Network**_ to include a variety of tools and projects not just Elastic Stack.
- [ ] Make multiple copies of each write-up to include the ability to use differing OS's (FreeBSD, Ubuntu, RedHat, Windows, MacOS)

# Current Rough-Drafts
- [ ] ElasticStackInstall-Ubuntu.pdf
- [ ] ElasticStackSecuritySetup.pdf
